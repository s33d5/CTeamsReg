﻿using System;
using Microsoft.Win32;
using System.Security.Principal;
using System.Diagnostics;
using Microsoft.Win32.TaskScheduler;
using System.IO;
namespace CTeamsTReg
{
    class Program
    {

        static void Main(string[] args)
        {
            Console.WriteLine("Writing to registry...");

            RegistryKey key;

            string otherUserID = "WTPCUser";
            string localPath = "C:\\TeamsLogout";
            string clientLogoutScriptPath = localPath + "\\TeamsLogout.ps1";
            string clientJsonPath = localPath + "\\desktop-config.json";

            //Write to local machine to block workspace join
            try
            {
                Console.WriteLine("Attempting Local Machine write...");
                key = Registry.LocalMachine.CreateSubKey("SOFTWARE", true).CreateSubKey("Policies", true).CreateSubKey("Microsoft", true).CreateSubKey("Windows", true).CreateSubKey("WorkplaceJoin", true);
                key.SetValue("BlockAADWorkplaceJoin", "1", RegistryValueKind.DWord);
                Console.WriteLine("Success.");

            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot write to Local Machine:");
                Console.WriteLine(e);
            }

            //write to assigned user to skip saved credentials and auto login in teams
            try
            {
                var account = new NTAccount(otherUserID);
                var identifier = (SecurityIdentifier)account.Translate(typeof(SecurityIdentifier));
                var sid = identifier.Value;

                Console.WriteLine("Attempting " + otherUserID + " write...");
                key = Registry.Users.CreateSubKey(sid).CreateSubKey("SOFTWARE", true).CreateSubKey("Microsoft", true).CreateSubKey("Office", true).CreateSubKey("Teams", true);
                key.SetValue("SkipUpnPrefill", "1", RegistryValueKind.DWord);
                Console.WriteLine("Success.");
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot write to" + otherUserID + ":");
                Console.WriteLine(e);
            }

            //Copy the logout script along with json file to c:
            //create tasks for auto logout at 9pm and at idle
            try
            {
                //check if directory and file exist
                Console.WriteLine("Attempting to copy Teams logout script...");
                if(!Directory.Exists(localPath))
                    Directory.CreateDirectory(localPath);
                if(File.Exists(clientLogoutScriptPath))
                {
                    Console.WriteLine("Teams script exists, deleting....");
                    File.Delete(clientLogoutScriptPath);
                }
                //copy file
                File.Copy("TeamsLogout\\TeamsLogout.ps1", clientLogoutScriptPath);
                Console.WriteLine("Teams logout script copy success.");

                //check if json already exists
                Console.WriteLine("Attempting to copy desktop-config.json...");
                if (File.Exists(clientJsonPath))
                {
                    Console.WriteLine("Json file exists, deleting...");
                    File.Delete(clientJsonPath);
                }
                File.Copy("TeamsLogout\\desktop-config.json", clientJsonPath);
                Console.WriteLine("Json copy success.");

                //make the current user powershell access unrestricted
                Console.WriteLine("Attempting to change user powershell access to unrestricted...");

                try
                {
                    var account = new NTAccount(otherUserID);
                    var identifier = (SecurityIdentifier)account.Translate(typeof(SecurityIdentifier));
                    var sid = identifier.Value;
                    
                    key = Registry.Users.CreateSubKey(sid).CreateSubKey("SOFTWARE", true).CreateSubKey("Microsoft", true).CreateSubKey("Powershell", true).CreateSubKey("1", true).CreateSubKey("ShellIds", true).CreateSubKey("Microsoft.Powershell", true);
                    key.SetValue("ExecutionPolicy", "Unrestricted", RegistryValueKind.String);
                    Console.WriteLine("Success.");
                }
                catch(Exception e)
                {
                    Console.WriteLine("Unable to access registry for powershell access:");
                    Console.WriteLine(e);
                }

                //create tasks
                Console.WriteLine("Attempting to create 9pm logout task for Teams logout...");


                using (TaskService ts = new TaskService())
                {
                    //delete task if exists already
                    ts.RootFolder.DeleteTask("TeamsLogoutPm", false);
                    //create 9pm task
                    TaskDefinition taskDef = ts.NewTask();
                    taskDef.RegistrationInfo.Description = "Auto logout teams at 9pm.";

                    //assign to other user
                    taskDef.Principal.LogonType = TaskLogonType.InteractiveToken;
                    taskDef.Principal.UserId = otherUserID;

                    //set trigger everyday at 9pm
                    DailyTrigger dt = (DailyTrigger)taskDef.Triggers.Add(new DailyTrigger(1));
                    dt.StartBoundary = new DateTime(2020, 07, 07, 21, 00, 00);

                    //wake up pc to run it
                    taskDef.Settings.WakeToRun = true;

                    //set action to launch powershell script
                    taskDef.Actions.Add(new ExecAction("powershell.exe", clientLogoutScriptPath));
                    
                    //add task to scheduler
                    TaskService.Instance.RootFolder.RegisterTaskDefinition("TeamsLogoutPm", taskDef);


                }
                Console.WriteLine("Success.");

                //create idle logout task
                Console.WriteLine("Attempting to create teams idle logout task...");
                using (TaskService ts = new TaskService())
                {
                    //delete task if already exists
                    ts.RootFolder.DeleteTask("TeamsLogoutSleep", false);

                    //create new task
                    TaskDefinition taskDef = ts.NewTask();
                    taskDef.RegistrationInfo.Description = "Auto logout teams during idle.";

                    //assign to other user
                    taskDef.Principal.LogonType = TaskLogonType.InteractiveToken;
                    taskDef.Principal.UserId = otherUserID;

                    //set to run at idle
                    taskDef.Settings.RunOnlyIfIdle = true;
                    taskDef.Settings.IdleSettings.StopOnIdleEnd = false;

                    // The amount of time that the computer must be in an idle state before the task is run.
                    taskDef.Settings.IdleSettings.IdleDuration = TimeSpan.FromMinutes(25);

                    //set action to launch powershell script
                    taskDef.Actions.Add(new ExecAction("powershell.exe", clientLogoutScriptPath));
                    taskDef.Triggers.Add(new IdleTrigger());

                    //add task to scheduler
                    TaskService.Instance.RootFolder.RegisterTaskDefinition("TeamsLogoutSleep", taskDef);
                }

                Console.WriteLine("Success.");
            }catch(Exception e)
            {
                Console.WriteLine("Cannot create scheduled Teams logout task:");
                Console.WriteLine(e);
            }
            
 
            
            Console.WriteLine("Complete. Press any key to exit...");
            Console.ReadKey();
        }
    }
}
